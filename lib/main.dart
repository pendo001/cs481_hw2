import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    Widget titleSection = Container(
      padding: const EdgeInsets.all(20), //creates padding around the outside
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Eiffel Tower', style: TextStyle (
                    fontWeight: FontWeight.bold,
                  ),
                  ),
                ),
                Text('Paris, France', style: TextStyle(
                  color: Colors.pinkAccent[200],),
                ),
              ],
            ),
          ),
          Icon(Icons.favorite_border , color: Colors.pinkAccent,),
          Text('120'),
        ],
      ),
    ); //Title Section

    Widget titleSection2 = Container(
      padding: const EdgeInsets.all(20), //creates padding around the outside
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Palace of Versailles', style: TextStyle (
                    fontWeight: FontWeight.bold,
                  ),
                  ),
                ),
                Text('Versailles, France', style: TextStyle(
                  color: Colors.pinkAccent[200],),
                ),
              ],
            ),
          ),
        ],
      ),
    ); //Title Section2


    Widget buttonSection = Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.pink, Icons.arrow_back, 'Back'),
          _buildButtonColumn(Colors.black54, Icons.home, 'Home'),
          _buildButtonColumn(Colors.pink, Icons.arrow_forward, 'Next'),
        ],
      ),
    );//Button Section

    Widget textSection = Container(
      padding: const EdgeInsets.all(20),
          child: Text(
        'The summer of 2018, my family decided to go to take a trip around Europe.'
        'We visited Ireland, England, France, and Italy. We each had a favorite place '
        'and mine was Paris, France. I have been wanting to go to France since I was  '
        'little. It was always a dream of mine and I was the designated translator since '
        'I took three years of French. We spent three days there and it was amazing. '
        'My favorite spots that we visited was the Eiffel Tower and the Palace of Versailles.',
        softWrap: true,
    ),
    );//Text Section

    Widget textSection2 = Container(
      padding: const EdgeInsets.all(20),
      child: Text(
        'The Palace of Versailles, also known as Versailles was really fancy. '
        'It was originally a hunting lodge and King Louis XIV decided to '
        'move the entire court and governmet there so he could have more '
        'control. It was absolutely beautiful, one of my favorite movies '
        'is Marie Antoinette which is about her life at Versailles. It is '
        'one thing to see Versailles in the movies and to see it in real life.',
        softWrap: true,
      ),
    );//Text Section2

    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.pinkAccent,
          title: Text('Layout Homework'),
        ),
        body: ListView(
          children: [
            buttonSection,
            Image.asset(
              'images/eiffel.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection,
            textSection,
            Image.asset(
              'images/versailles.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection2,
            textSection2,
          ]
        ),
        ),
      );



  } //BUILD End Bracket

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            )
          )
        ),
      ],
    );
  } //Button Struct

} //MY APP